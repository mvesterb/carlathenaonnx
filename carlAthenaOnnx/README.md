# CARL Athena Onnx

The following package is an AthDerivation analysis algorithm that 
processes EVNT (HepMC) files using the ATHENA environment to create
during run time xAOD style physics objects. This allows the calculation of 
final state observable kinematics analogous to reconstructed detector quantities
for use with the CARL multivariate re-weighting technique:

https://arxiv.org/pdf/1506.02169.pdf
 
This package contains two key components:

*   **CarlDataGen:** Generates the input dataset to be used by the CARL pytorch Neural Network & calibration code base.
*   **CARLAthenaAlg:** Modularised code (C++ class) that utilises [OnnxRuntime](https://github.com/microsoft/onnxruntime) to deploy pytorch calibrated neural network architectures, so that Monte Carlo events can be re-weighted via a multiplicative scale to map between Monte Carlo generator configurations 


The latter, `CARLAthenaAlg` takes as an input models trained using the carl-torch package:

https://github.com/leonoravesterbacka/carl-torch
 
which as summarised above, are saved in an `onxx` format to be deployed in the run time environment using onnxruntime. 

## Installation

Create a base directory (`<masterDir>`) for storing `source` code, `compiled` binaries, and a `run` work environment:

```
mkdir <masterDir>
cd <masterDir>
mkdir source run build
```

Then please clone the gitlab repository into `source/` directory using:

```
cd source
git clone ssh://git@gitlab.cern.ch:7999/mvesterb/carlathenaonnx.git .
```

### Building code

To build the code please re-locate to the build directory, `<masterDir>/build`, and then execute:

```bash
cd <masterDir>/build 
asetup AthDerivation,21.2.95.0,here
/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.17.3/Linux-x86_64/bin/cmake -DATLAS_FORCE_PLATFORM=x86_64-slc6-gcc62-opt -DCMAKE_INSTALL_PREFIX=/InstallArea/x86_64-slc6-gcc62-opt ../source
/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.17.3/Linux-x86_64/bin/cmake --build . 
source x86_64-slc6-gcc62-opt/setup.sh
```

#### WARNING!
It should be noted that the `DATLAS_FORCE_PLATFORM` and `DCMAKE_INSTALL_PREFIX` options are necessary due to the *spare-checkout* of Athena within the source directory, and then manual compilation of `OnnxRunTime`. Specifically, one can find in the package the [carlathenaonnx/cmake](https://gitlab.cern.ch/mvesterb/carlathenaonnx/-/tree/master/cmake) build instructions that contains a a *'find* and *'build'* cmake instruction that is executed during the above `cmake ../source` & `cmake --build .` instruction above. This instruction will [find the onnxruntime](https://gitlab.cern.ch/mvesterb/carlathenaonnx/-/blob/master/cmake/Findonnxruntime.cmake#L17) code base inside *'AtlasExternals'* module, and then [build the onnxruntime](https://gitlab.cern.ch/mvesterb/carlathenaonnx/-/blob/master/cmake/BuildOnnxRuntime.cmake#L54) source code. Unfortunately, as can be seen in the [cmake/BuildOnnxRuntime.cmake](https://gitlab.cern.ch/mvesterb/carlathenaonnx/-/blob/master/cmake/BuildOnnxRuntime.cmake#L56) file, the `${ATLAS_PLATFORM}` environment variable is used as the default install location.

Unfortunately, the `${ATLAS_PLATFORM}` environment variable is set during the build stage twice, once at the start during execution of `setupATLAS; asetup AthDerivation,21.2.95.0,here;` and during the `onnxruntime buildinstall` step. The former is the default platform of the machine that the user is utilising, whilst the latter is extracted from the base project version of the `AtlasExternals/AtheDerivation` package. This can sometimes cause conflicts because the user builds on a Centos7 machine, therefore ends up with a `x86_64-centos7-gcc62-opt/` install area during the `setupATLAS/asetup` stage, however the `AtlasExternals/AthDerivation` packages being built in the spare-checkout athena environment are sometimes built against a SLC6 platform, and so the onnxruntime installion is set to `x86_64-slc6-gcc62-op/`. Consequently, in this instance the user can naturally perform the following: 

```bash
cd <masterDir>/build 
asetup AthDerivation,21.2.95.0,here
/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.17.3/Linux-x86_64/bin/cmake ../source   
/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.17.3/Linux-x86_64/bin/cmake --build .   
cp ./x86_64-slc6-gcc62-opt/lib/libonnxruntime.so x86_64-centos7-gcc62-opt/lib/
cp ./x86_64-slc6-gcc62-opt/lib/libonnxruntime.so.1.1.1 x86_64-centos7-gcc62-opt/lib/
/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.17.3/Linux-x86_64/bin/cmake --build .   
source x86_64-centos7-gcc62-opt/setup.sh #sjiggins
```

Where one copies the onnxruntime compiled binaries/library files to the default platform install area of `x86_64-centos7-gcc62-opt/`.

This will not be needed in the future as the [Athena !33047](https://gitlab.cern.ch/atlas/athena/-/merge_requests/33047) merge request was approved that now adds from [AtlasExternals](https://gitlab.cern.ch/atlas/atlasexternals) the onnxruntime code base. From this MR one can see that the change log shows that the [Projects/AthDerivation/externals.txt](https://gitlab.cern.ch/atlas/athena/-/merge_requests/33047/diffs#diff-content-88e1031609194fcd693858c3bfcb7e48e03e72b7) file was changed to a new [AthDerivationExternals](https://gitlab.cern.ch/atlas/atlasexternals/-/tree/master/Projects%2FAthDerivationExternals) version, where this new version contains inside the [package_filter.txt](https://gitlab.cern.ch/atlas/atlasexternals/-/blob/master/Projects/AthDerivationExternals/package_filters.txt) an instance of *External/onnxruntime*.

## Local running

To run an instance of an athena algorithm within the ATHENA environment please move
into the run directory and execute:

```
cd <masterDir>/run
athena CARLAthena/CARLAthenaAlgJobOptions.py 2>&1 | tee RunTime.log
```

## Grid submission

In order to run on the GRID the user must have the following:

1.  A valid GRID certifcate
2.  Signed the ATLAS VO usage policy

Assuming the above build instructions have been followed, the submission is executed after following the steps below:

1.  **Setup Athena Environment:**
    1. cd build/
    2. SetupATLAS
    3. asetup
    4. source x86_64-centos7-gcc62-opt/setup.sh
2.  **Setup Rucio:**
    1. cd ../
    2. lsetup rucio
    3. voms-proxy-init -voms atlas
3.  **Setup Panda:**
    1. lsetup panda
4.  **Submission:**
    1. mkdir -p submit/
    2. cd submit/
    3. python ../source/carlAthenaOnnx/submit/submitJobs.py 
 

