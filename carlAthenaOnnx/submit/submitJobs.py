import os
import sys

##########################################################################################################################
suffix    =".test"
username  ="mvesterb"
inputFile ="/afs/cern.ch/work/m/mvesterb/public/carlAthena/june17/source/carlAthenaOnnx/submit/inputlist_2017_vjets_sherpa221.txt"

##########################################################################################################################


###########################################################################################################################
dsList=open(inputFile,'r')
lines=dsList.readlines()

def submitJob(ds) :

    ##Athena Job Options
    com = "pathena CARLAthena/CARLAthenaAlgJobOptions.py "   
    
    ## Notify Panda that submission will use new code package 
    com += "--useNewCode "

    ## Notify panda to use input datasets in configuration file
    com += "--inDS " + ds + " "

    ## Notify panda that I will either use user rights or official production role
    oDS = "user."+username+"."+ds.replace("/","")+suffix

    print oDS+"  has length: "+str(len(oDS))
    while len(oDS) > 115 :
        print len(oDS)," too long!!!"
        splODS=oDS.split("_")
        splODS.pop(2)
        oDS="_".join(splODS)
        pass
    print "final: "+oDS+"  has length: "+str(len(oDS))
    com += "--outDS "+ oDS + " "

    ##Optional output name (ignore)
    #com += " --extOutFile ntuple.root "

    ## Unknown option at this point, but lets keep it.
    com += " --supStream GLOBAL "

    ## ptions for each job submission
    #com += " --nEventsPerJob 25000 "
    #com += " --nFilesPerJob 1"

    ## Notify panda submission to merge all sub-jobs at GRID site.
    com += " --mergeOutput "
    ## Return the command to be executed
    return com
############################################################################################################################################




############################################################################################################################################
############################################################################################################################################
############################################################################################################################################
for ds in lines :
    if "#" in ds : continue
    if "mc" not in ds and "valid" not in ds and "group" not in ds and "data" not in ds: continue

    print " "
    print "///////////////////////////////////////////////////////////////////////////////////////"
    print ds
    command=submitJob(ds.replace("\n",""))
    print command
    os.system(command)
    pass

