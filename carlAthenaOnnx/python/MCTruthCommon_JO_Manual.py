# CopyrightOA (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

print("<MCTruthCommon_JO>::   Setting JetRecFalgs for truth setup")

## Method : Initialise jet reco algorithm in main Athena Sequence 
def initialiseJetAlgs(kernel=None, CfgMgr=None, ToolSvc=None):
    
    ## Jet Flags
    from JetRec.JetRecFlags import jetFlags
    jetFlags.useTruth = True
    jetFlags.useTracks = False
    jetFlags.truthFlavorTags = ["BHadronsInitial", "BHadronsFinal", "BQuarksFinal",
                                "CHadronsInitial", "CHadronsFinal", "CQuarksFinal",
                                "TausFinal",
                                "Partons",
                                ]
    
    ## Call jet reco alg.
    from JetRec.JetRecStandard import jtm
    
    ## Add jet algorithms
    from JetRec.JetAlgorithm import addJetRecoToAlgSequence
    addJetRecoToAlgSequence(kernel,eventShapeTools=None)
    
    
    ## Configure MCTruthClassifier, actually just using default properties
    ToolSvc += CfgMgr.MCTruthClassifier("JetMCTruthClassifier") 


## Method : Add truth jets for EVNT
def addTruthJetsEVNT(kernel=None, CfgMgr=None, ToolSvc=None):
 
    ## Call jet reco alg.
    from JetRec.JetRecStandard import jtm
    ## Jet algorithm
    from JetRec.JetRecConf import JetAlgorithm

    ## Define Truth modifiers for jets
    truth_modifiers = [jtm.truthpartondr, jtm.removeconstit, 
                       jtm.partontruthlabel, jtm.jetdrlabeler, 
                       jtm.trackjetdrlabeler]
    
    #####--------------------------------------------------------------------
    ##### Antikt2 Truth Charged Jets
    ##### Configure a jet builder, which will make xAOD::Jet from a PseudoJet
    ##### Use all default values here
    ###ToolSvc += CfgMgr.JetFromPseudojet("AntiKt2TruthChargedJetBuild")
    ##### Configure a jet finder, which actually does the AntiKt on the PseudoJets,
    ##### but also then use the builder to make the xAOD::Jet
    ###ToolSvc += CfgMgr.JetFinder("AntiKt2TruthChargedJetFind",
    ###                           JetAlgorithm="AntiKt",
    ###                           JetRadius = 0.2,
    ###                           PtMin=5000.,
    ###                           JetBuilder=ToolSvc.AntiKt2TruthChargedJetBuild
    ###                           )
    ##### Configure JetRecTool to actually execute the JetFinder on the given PseudoJets
    ###ToolSvc += CfgMgr.JetRecTool("AntiKt2TruthChargedJetRecTool",
    ###                             OutputContainer="AntiKt2TruthChargedJets",
    ###                             PseudoJetGetters=[ToolSvc.truthchargedget],
    ###                             JetFinder=ToolSvc.AntiKt2TruthChargedJetFind
    ###                             )
    ###
    ###
    #####ToolSvc.AntiKt2TruthChargedJetRecTool.JetModifiers = buildModifiers(truth_modifiers)   ##Not working
    ###kernel += CfgMgr.JetAlgorithm(Tools=[ToolSvc.truthpartcharged,ToolSvc.AntiKt2TruthChargedJetRecTool])
    
    akt2truthcharged = jtm.addJetFinder("AntiKt2TruthChargedJets", "AntiKt", 0.2, 
                                        "truthcharged", 
                                        modifiersin=[jtm.truthpartondr, jtm.partontruthlabel, jtm.removeconstit, 
                                                     jtm.jetdrlabeler, jtm.trackjetdrlabeler], 
                                        ptmin= 5000)
    akt2truthchargedalg = JetAlgorithm("jetalgAntiKt2TruthChargedJets", Tools = [akt2truthcharged] )
    kernel += akt2truthchargedalg
    #####--------------------------------------------------------------------

    #####--------------------------------------------------------------------
    #####  AntiKtVR30Rmax4Rmin02Track collection using Truth information
    ## Define the VR job options
    VRJetOptions = dict( ghostArea = 0 , ptmin = 2000,
                         variableRMinRadius = 0.02, variableRMassScale = 30000,
                         calibOpt = "none")
    ## Add jet finder alg and JetRecTool() using 'addJetFinder' method
    aktVR30Rmax4Rmin02truthcharged = jtm.addJetFinder("AntiKtVR30Rmax4Rmin02TruthChargedJets", "AntiKt", 0.4, 
                                                      "truthcharged", 
                                                      modifiersin=[jtm.truthpartondr, jtm.partontruthlabel, jtm.removeconstit, 
                                                                   jtm.jetdrlabeler, jtm.trackjetdrlabeler], 
                                                      **VRJetOptions)
    aktVR30Rmax4Rmin02truthchargedalg = JetAlgorithm("jetalgAntiKtVR30Rmax4Rmin02TruthChargedJets", Tools = [aktVR30Rmax4Rmin02truthcharged] )
    kernel += aktVR30Rmax4Rmin02truthchargedalg
    #####--------------------------------------------------------------------

    #####--------------------------------------------------------------------
    #### AntiKt2 truth charged jets ghost association
    ###from JetRec.JetRecConf import PseudoJetGetter
    ###jtm += PseudoJetGetter(
    ###    "gakt2truthchargedget", # give a unique name
    ###    InputContainer = "AntiKt2TruthChargedJets", # SG key
    ###    Label = "GhostAntiKt2TruthChargedJets",   # this is the name you'll use to retrieve associated ghosts
    ###    OutputContainer = "PseudoJetGhostAntiKt2TruthChargedJet",
    ###    SkipNegativeEnergy = True,
    ###    GhostScale = 1.e-20,   # This makes the PseudoJet Ghosts, and thus the reco flow will treat them as so.
    ###    )
    ###
    ###trackjetgetters = []
    ###trackjetgetters += [jtm.gakt2truthchargedget]
    ###truthgetters = [jtm.truthget]
    ###truthgetters += trackjetgetters
    ###flavorgetters = []
    ###for ptype in jetFlags.truthFlavorTags():
    ###    flavorgetters += [getattr(jtm, "gtruthget_" + ptype)]
    ###truthgetters   += flavorgetters
    ###print 'jtm.gettersMap = ', jtm.gettersMap["truth"]
    ###jtm.gettersMap["truth"]   = list(truthgetters)
    ###print 'jtm.gettersMap = ', jtm.gettersMap["truth"]
    #####--------------------------------------------------------------------

    ########--------------------------------------------------------------------
    ##### AntiKt10 Truth Trimmed
    ###akt10 = jtm.addJetFinder("AntiKt10TruthJets", "AntiKt", 1.0, "truth",ptmin= 200000)
    ###akt10alg = JetAlgorithm("jetalgAntiKt10TruthJets", Tools = [akt10] )
    ###kernel += akt10alg
    ###akt10trim = jtm.addJetTrimmer("TrimmedAntiKt10TruthJets", rclus=0.2, ptfrac=0.05, input='AntiKt10TruthJets', modifiersin=[jtm.nsubjettiness, jtm.removeconstit])
    ###akt10trimalg = JetAlgorithm("jetalgTrimmedAntiKt10TruthJets", Tools = [akt10trim] )
    ###kernel += akt10trimalg
    ###akt10WZtrim = jtm.addJetTrimmer("TrimmedAntiKt10TruthWZJets", rclus=0.2, ptfrac=0.05, input='AntiKt10TruthWZJets', modifiersin=[jtm.nsubjettiness, jtm.removeconstit])
    ###akt10WZtrimalg = JetAlgorithm("jetalgTrimmedAntiKt10TruthWZJets", Tools = [akt10WZtrim] )
    ###kernel += akt10WZtrimalg
    ########--------------------------------------------------------------------

## Method : Add truth jets for EVNT
def addTruthLargeTrimJetsEVNT(kernel=None, CfgMgr=None, ToolSvc=None):
 
    ## Call jet reco alg.
    from JetRec.JetRecStandard import jtm
    ## Jet algorithm
    from JetRec.JetRecConf import JetAlgorithm

    ## Define Truth modifiers for jets
    truth_modifiers = [jtm.truthpartondr, jtm.removeconstit, 
                       jtm.partontruthlabel, jtm.jetdrlabeler, 
                       jtm.trackjetdrlabeler]
    
    #####--------------------------------------------------------------------
    ## AntiKt10 Truth Trimmed
    akt10 = jtm.addJetFinder("AntiKt10TruthJets", "AntiKt", 1.0, "truth",ptmin= 200000)
    akt10alg = JetAlgorithm("jetalgAntiKt10TruthJets", Tools = [akt10] )
    kernel += akt10alg
    akt10trim = jtm.addJetTrimmer("TrimmedAntiKt10TruthJets", rclus=0.2, ptfrac=0.05, input='AntiKt10TruthJets', modifiersin=[jtm.nsubjettiness, jtm.removeconstit])
    akt10trimalg = JetAlgorithm("jetalgTrimmedAntiKt10TruthJets", Tools = [akt10trim] )
    kernel += akt10trimalg
    #####--------------------------------------------------------------------
    

# Helper for adding truth jet collections
def addTruthJets(kernel=None, CfgMgr=None, ToolSvc=None):

    ## Call jet reco alg.
    from JetRec.JetRecStandard import jtm
    ## Jet Flags
    from JetRec.JetRecFlags import jetFlags
    ## Jet Algorithm
    from JetRec.JetRecConf import JetAlgorithm
    
    ##Check JetRecStandard jtm reco alg. for truthpartcharged truth collection
    if not hasattr(jtm,'truthpartcharged'):
        # make sure if we are using EVNT that we don't try to check sim metadata 
        barCodeFromMetadata=0
        from ParticleJetTools.ParticleJetToolsConf import CopyTruthJetParticles
        jtm += CopyTruthJetParticles("truthpartcharged", OutputName="JetInputTruthParticlesCharged",   ##MMaybe ToolS
                                     MCTruthClassifier=jtm.JetMCTruthClassifier,
                                     ChargedParticlesOnly=True,
                                     IncludePromptLeptons=False  #change to False for 'WZ' truth jets
                                    )
        # Add a jet tool runner for this thing
        from JetRec.JetRecConf import JetToolRunner,PseudoJetGetter
        jtm += JetToolRunner("jetchargedrun", EventShapeTools=[], Tools=[jtm.truthpartcharged], 
                             Timer=jetFlags.timeJetToolRunner() )
        
        # Define the Pseudo-vector collection and define tha clustering algorithm
        kernel += JetAlgorithm("jetchargedalg")
        jetchargedalg = kernel.jetchargedalg
        jetchargedalg.Tools = [ jtm.jetchargedrun ]
        jtm += PseudoJetGetter(
            "truthchargedget",
            Label = "TruthCharged",
            InputContainer = jtm.truthpartcharged.OutputName,
            OutputContainer = "PseudoJetTruthCharged",
            GhostScale = 0.0,
            SkipNegativeEnergy = True
            )
        jtm.gettersMap['truthcharged'] = [jtm.truthchargedget]
        addTruthJetsEVNT(kernel, CfgMgr, ToolSvc)

        
    ## Trimmed Anti-Kt WZ and non-WZ jets
    ##addTruthLargeTrimJetsEVNT(kernel, CfgMgr, ToolSvc)

