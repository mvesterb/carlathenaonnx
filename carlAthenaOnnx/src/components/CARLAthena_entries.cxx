
#include "GaudiKernel/DeclareFactoryEntries.h"

#include "../CARLAthenaAlg.h"

DECLARE_ALGORITHM_FACTORY( CARLAthenaAlg )

DECLARE_FACTORY_ENTRIES( CARLAthena ) 
{
  DECLARE_ALGORITHM( CARLAthenaAlg );
}
