#ifndef CARLATHENA_CARLATHENAALG_H
#define CARLATHENA_CARLATHENAALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

// ROOT includes
#include <TTree.h>
#include <vector>
#include <TH1.h>
#include "TLorentzVector.h"

// Basic C++ STD library includes
#include <map>
#include <iostream>
#include <memory>
#include <fstream>

// xAOD includes
#include <xAODMuon/MuonContainer.h>

#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"

#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthVertexContainer.h"

#include "xAODTruth/TruthMetaData.h"
#include "xAODTruth/TruthMetaDataContainer.h"

#include "xAODJet/Jet.h"
#include "xAODJet/JetContainer.h"

#include "xAODMissingET/MissingET.h"
#include "xAODMissingET/MissingETContainer.h"

// OnnxRuntime include(s).
//#include <core/session/onnxruntime_cxx_api.h>

// CARLNN includes
#include "CARLNN.h"

class CARLAthenaAlg: public ::AthAnalysisAlgorithm {
 public:
  CARLAthenaAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~CARLAthenaAlg();

  //AthAnalysisAlgorithm base components
                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  virtual StatusCode  execute();        //per event
  virtual StatusCode  finalize();       //once, after all events processed


  unsigned int m_runNumber = 0; ///< Run number
  unsigned long long m_eventNumber = 0; ///< Event number

  ///Other useful methods provided by base class are:
  ///evtStore()        : ServiceHandle to main event data storegate
  ///inputMetaStore()  : ServiceHandle to input metadata storegate
  ///outputMetaStore() : ServiceHandle to output metadata storegate
  ///histSvc()         : ServiceHandle to output ROOT service (writing TObjects)
  ///currentFile()     : TFile* to the currently open input file
  ///retrieveMetadata(...): See twiki.cern.ch/twiki/bin/view/AtlasProtected/AthAnalysisBase#ReadingMetaDataInCpp


 private:

  bool m_DoSherpaVpT = false;
  bool m_DoSherpattbar = false;
  bool m_DoInference = false;
  bool m_DoDebug = false;

  //tree and branches
  TString nameX;
  void ClearBranches();
  TTree* Tree;
  int m_Njets;
  float m_MET;
  float m_HT;
  float m_VpT;
  float m_Veta;
  float m_weight;
  std::vector<float> m_Jet_Pt;
  std::vector<float> m_Jet_Eta;
  std::vector<float> m_Jet_Phi;
  std::vector<float> m_Jet_Mass;
  int m_nSignalLeptons;
  std::vector<float> m_Lepton_Pt;
  std::vector<float> m_Lepton_Eta;
  std::vector<float> m_Lepton_Phi;
  std::vector<float> m_Lepton_Mass;
  std::vector<int> m_Lepton_ID;
  int nEvent_0L = 0;
  int nEvent_1L_el = 0;
  int nEvent_1L_mu = 0;
  int nEvent_2L_el = 0;
  int nEvent_2L_mu = 0;
  int nEvent_2L_emu = 0;
  //CARLNN object
  CARLNN* m_CARLNN; //Pointer as want to store on heap due to potential size issue
  //std::map<std::string, float> m_input_tensor_values;
  std::unordered_map<std::string, float> m_input_tensor_values;
  int LeptonSelection(std::vector< const xAOD::TruthParticle* > & SignalLeptons);
  void SaveAllLeptons(std::vector< const xAOD::TruthParticle* > & SignalLeptons);

  xAOD::TruthParticleContainer* truthElectrons = 0;
  xAOD::TruthParticleContainer* truthMuons = 0;
  const xAOD::JetContainer * truthJets = 0;
  std::vector< std::shared_ptr<xAOD::Jet> > SignalJets;
  //std::vector< xAOD::Jet* > SignalJets;
  bool PassLooseElectron(const xAOD::TruthParticle * el);
  bool PassLooseMuon(const xAOD::TruthParticle * mu);
  std::vector<TLorentzVector> SignalJets_Vec;
  //static bool sort_pt(const xAOD::Jet* jetA, const xAOD::Jet* jetB) {
  static bool sort_pt( std::shared_ptr<xAOD::Jet> jetA, std::shared_ptr<xAOD::Jet> jetB) {
    return jetA->pt() > jetB->pt();
  }



  /// User specified data member
  std::vector<double> inputs;
  //std::map<std::string, std::map<std::string, double> > inputs;
  std::map<std::string, std::map<std::string, std::vector<double>> > input_sequences;
  std::string m_sampleName;
  std::string in_file_name;
  std::stringstream paths;
  std::string segment;
  std::string fileLocation;
  std::ifstream infile;
  std::ifstream input;
  std::string output_main;
  std::string DataPath;
  TLorentzVector MET_Vec;
  TLorentzVector MET_Vec_Nu;
  const xAOD::JetContainer * jets = nullptr;
  const xAOD::MissingETContainer* truthMET = nullptr;
  const xAOD::TruthParticleContainer* truthParticles = nullptr;
  TLorentzVector GetDecayProducts(const xAOD::TruthParticle* mother,
                                  std::set<int> * W_decay_products,
                                  bool W_flag, TLorentzVector W_Vec);

};

#endif //> !CARLATHENA_CARLATHENAALG_H
